angular.module('calltaxi', ['ionic', 'ffm.controllers'])

        .run(function($rootScope, $state, $ionicPlatform, $window) {
			$rootScope.config = {
                url: 'https://api.parse.com/1/classes/Sam'
            };
           
            $ionicPlatform.ready(function() {

                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }			
            });

        })

        .config(function($stateProvider, $urlRouterProvider) {
            $stateProvider
                    
					.state('app', {
                        url: "/app",
                        abstract: true,
                        templateUrl: "templates/menu.html",
                        controller: "AppCtrl"
                    })
					
					
					.state('app.online', {
                        url: "/online",
                        
						views: {
                            'menuContent': {
                                templateUrl: "templates/online.html",
								controller: "OnlineCtrl"
                            },
                            'headLogo': {
                                templateUrl: 'templates/logo.html'
                            }
                        }
                    })
					
					.state('app.contact', {
                        url: "/contact/:id",
                        
						views: {
                            'menuContent': {
                                templateUrl: "templates/contact.html",
								controller: "ContactCtrl"
                            },
                            'headLogo': {
                                templateUrl: 'templates/logo.html'
                            }
                        }
                    })
					.state('app.detail', {
                        url: "/detail/:id",
                        
						views: {
                            'menuContent': {
                                templateUrl: "templates/details.html",
								controller: "DetailCtrl"
                            },
                            'headLogo': {
                                templateUrl: 'templates/logo.html'
                            }
                        }
                    })
                   



            // fallback route
            $urlRouterProvider.otherwise('/app/online');

        });
