angular.module('globaltpl', [])

        .factory('GlobalTpl', function($http, $ionicLoading, $ionicPopup) {

            function showLoading() {
                $ionicLoading.show({
                    content: 'Loading'
                });
            }

            function hideLoading() {
                $ionicLoading.hide();
            }

            function showAlert() {
                $ionicPopup.alert({
                    title: 'Connection error',
                    template: 'Internet connection error. Please try again later!'
                });
            }

            function request(options, callback, final) {

                var config = {
                    timeout: 15000
                };

                if (options.showLoad) {
                    showLoading();
                }

                if (options.method === 'post') {
                    $http.post(options.url, options.data, config).
                            success(function(response) {

                                if (response) {
                                    // Execute the callback
                                    if ((typeof callback !== 'undefined') && (typeof callback === 'function')) {
                                        callback(response);
                                    }
                                }

                            }).
                            error(function(response) {
                                showAlert();
                            }).
                            finally(function() {
                                hideLoading();
                                if ((typeof final !== 'undefined') && (typeof final === 'function')) {
                                    final();
                                }
                            });


                    ;

                } else if ((options.method === 'get') || (options.method === 'delete')) {
                    var method = $http[options.method];
                    method(options.url, options.headers, config).
                            success(function(response) {
                                // Implement callback
                                if ((typeof callback !== 'undefined') && (typeof callback === 'function')) {
                                    callback(response);
                                }

                            }).
                            error(function() {
                                showAlert();
                            }).
                            finally(function() {
                                if (options.showLoad) {
                                    hideLoading();
                                }

                                if ((typeof final !== 'undefined') && (typeof final === 'function')) {
                                    final();
                                }
                            });
                }
            }


            return {
                showLoading: showLoading,
                hideLoading: hideLoading,
                request: request
            };

        });
