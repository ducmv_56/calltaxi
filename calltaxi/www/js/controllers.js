// Angular controllers
angular.module('ffm.controllers', ['ngCordova'])
	.controller("AppCtrl", function($scope, $rootScope, $state, $location, $window, $http) {
		$scope.items = [];
		$scope.namecity = '';
		$scope.list = [];
		$window.navigator.geolocation.getCurrentPosition(function(position){
			var lat = position.coords.latitude;
			var lng = position.coords.longitude;
			$http({method : 'GET', url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng +'&sensor=true'})
			.success(function(data, status){
			$scope.items = data.results;
			angular.forEach($scope.items[0].address_components, function(item){
				if(item.types[0] === 'administrative_area_level_1'){
					$scope.namecity = item.long_name;						
				}});
			$http({method : 'GET',url : $rootScope.config.url + '?where={"name" : "'+$scope.namecity+'"}', headers: { 'X-Parse-Application-Id':'wTrC9S1Wu7TvW7zYAxQLAYLTuamxY83DYBtkiqsw', 'X-Parse-REST-API-Key':'lzyoI7MESvfmMnzW4nt2EvjkaBntHMiQrc9VuAEw'}})
            .success(function(data, status) {
				$scope.list = data.results;
				
			if($scope.list !== null){
				$location.path('/app/contact/'+ $scope.namecity);
			}else
				$location.path('/app/online');
			})
			}) 
			.error(function(data, status) {
                alert("Error");
            })
			.finally(function() {
			// Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
			});
		});
    })


    .controller("OnlineCtrl", function($scope, $rootScope, $ionicScrollDelegate, $http, $location) {
        $scope.title = 'Call Taxi';
        $scope.search = '';       
		$scope.items = [];
		
        function loadData() {
			$http({method : 'GET',url : $rootScope.config.url, headers: { 'X-Parse-Application-Id':'wTrC9S1Wu7TvW7zYAxQLAYLTuamxY83DYBtkiqsw', 'X-Parse-REST-API-Key':'lzyoI7MESvfmMnzW4nt2EvjkaBntHMiQrc9VuAEw'}})
            .success(function(data, status) {
				$scope.items = data.results;
            })
            .error(function(data, status) {
                alert("Error");
            })
			.finally(function() {
			// Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
			});
        }
        $scope.$on('stateChangeSuccess', function() {
            //$scope.loadMore();
        });

        // Filter methods
        $scope.scrollTop = function() {
            ionicScrollDelegate.scrollTop();
        };
        $scope.cancelSearch = function() {
            $scope.search = '';
        };
        $scope.getItems = function() {
            var resultArr = [];
            angular.forEach($scope.items, function(item) {
				var diff = true;
				for( var i = 0; i < resultArr.length; i++){
					if(item.name === resultArr[i].name){
						diff = false;
						break;
					}
				}
                var itemDoesMatch = !$scope.search ||
                    item.name.toLowerCase().indexOf($scope.search.toLowerCase()) > -1;
                if (itemDoesMatch && diff) {
                    resultArr.push(item);
                }
            });
            return resultArr;
        };
		
        $scope.doRefresh = function() {
            $scope.items = [];
            loadData();
        };
		$scope.contact = function(id) {
            $location.path('/app/contact/'+ id);
        };
        loadData();   
        })
		
		.controller("ContactCtrl", function($scope, $rootScope, $stateParams, $ionicScrollDelegate, $http, $ionicModal, $ionicPopup, $location) {
        $scope.title = $stateParams.id;
        $scope.search = '';
		$scope.items = {};
		$scope.id = $stateParams.id;
		console.log($stateParams.id);
		$scope.modalTitle = "";
		$scope.model = {};
        $scope.list = {};

		
        function loadData() {
			$http({method : 'GET',url : $rootScope.config.url  + '?where={"name" : "' +$stateParams.id+'"}', headers: { 'X-Parse-Application-Id':'wTrC9S1Wu7TvW7zYAxQLAYLTuamxY83DYBtkiqsw', 'X-Parse-REST-API-Key':'lzyoI7MESvfmMnzW4nt2EvjkaBntHMiQrc9VuAEw'}})
            .success(function(data, status) {				
				$scope.items = data.results;	
            })
            .error(function(data, status) {
                alert("Error");
            })
			.finally(function() {
			// Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
			});		
        }           
        
        $scope.$on('stateChangeSuccess', function() {
            //$scope.loadMore();
        });

        // Filter methods
        $scope.scrollTop = function() {
            ionicScrollDelegate.scrollTop();
        };
        $scope.cancelSearch = function() {
            $scope.search = '';
        };
		
        $scope.getItems = function() {
            var resultArr = [];
            angular.forEach($scope.items, function(item) {
                var itemDoesMatch = !$scope.search ||
                    item.name.toLowerCase().indexOf($scope.search.toLowerCase()) > -1;
                if (itemDoesMatch) {
                    resultArr.push(item);
                }
            });
            return resultArr;
        };			
		$scope.doRefresh = function() {
            $scope.items = [];
            loadData();
        };		
		$scope.goDetails = function(id) {
            $location.path('/app/detail/'+ id);
        };
        loadData();   
        })
		
	.controller("DetailCtrl", function($scope, $rootScope, $stateParams, $ionicScrollDelegate, $http, $location, $filter) {
        $scope.title = '';     
		$scope.list = {};
		
        function loadData() {
			$http({method : 'GET',url : $rootScope.config.url+ '/' + $stateParams.id, headers: { 'X-Parse-Application-Id':'wTrC9S1Wu7TvW7zYAxQLAYLTuamxY83DYBtkiqsw', 'X-Parse-REST-API-Key':'lzyoI7MESvfmMnzW4nt2EvjkaBntHMiQrc9VuAEw'}})
            .success(function(data, status) {
					$scope.list = data;
					$scope.title = $scope.list.nameTaxi + " Taxi";
            })
            .error(function(data, status) {
                alert("Error");
            })
			.finally(function() {
			// Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
			});
        }           
        
        $scope.$on('stateChangeSuccess', function() {
            //$scope.loadMore();
        });

        // Filter methods
        $scope.scrollTop = function() {
            ionicScrollDelegate.scrollTop();
        };
        $scope.phonecallTab = function(id) {
				var call = "tel:" + id;
				console.log(id);
				alert("Calling " + call);
				document.location.href = call;
		}
        $scope.doRefresh = function() {
            $scope.list = [];
            loadData();
        };
        loadData();   
        })	
		
		
		
		
		
		
		