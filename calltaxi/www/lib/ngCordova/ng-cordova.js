/*!
 * ngCordova
 * Copyright 2014 Drifty Co. http://drifty.com/
 * See LICENSE in this repository for license information
 */
(function() {

    angular.module('ngCordova', [
        'ngCordova.plugins'
    ]);

    angular.module('ngCordova.plugins.camera', [])

            .factory('$cordovaCamera', ['$q', function($q) {

                    return {
                        getPicture: function(options) {
                            var q = $q.defer();

                            if (!navigator.camera) {
                                q.resolve(null);
                                return q.promise;
                            }

                            navigator.camera.getPicture(function(imageData) {
                                q.resolve(imageData);
                            }, function(err) {
                                q.reject(err);
                            }, options);

                            return q.promise;
                        },
                        cleanup: function(options) {
                            var q = $q.defer();

                            navigator.camera.cleanup(function() {
                                q.resolve(arguments);
                            }, function(err) {
                                q.reject(err);
                            });

                            return q.promise;
                        }

                    }
                }]);

    angular.module('ngCordova.plugins.appAvailability', [])

            .factory('$cordovaAppAvailability', ['$q', function($q) {

                    return {
                        check: function(urlScheme) {
                            var q = $q.defer();

                            appAvailability.check(urlScheme, function(result) {
                                q.resolve(result);
                            }, function(err) {
                                q.reject(err);
                            });

                            return q.promise;
                        }
                    }
                }]);

    angular.module('ngCordova.plugins.device', [])

            .factory('$cordovaDevice', [function() {

                    return {
                        getDevice: function() {
                            return device;
                        },
                        getCordova: function() {
                            return device.cordova;
                        },
                        getModel: function() {
                            return device.model;
                        },
                        // Waraning: device.name is deprecated as of version 2.3.0. Use device.model instead.
                        getName: function() {
                            return device.name;
                        },
                        getPlatform: function() {
                            return device.platform;
                        },
                        getUUID: function() {
                            return device.uuid;
                        },
                        getVersion: function() {
                            return device.version;
                        }
                    }
                }]);

// TODO: writeFile needs work, doesn't function
// TODO: add support for readFile -> readAsData
// TODO: add support for readFile -> readAsBinaryString
// TODO: add support for readFile -> readAsArrayBuffer
// TODO: add functionality to define storage size in the getFilesystem() -> requestFileSystem() method
// TODO: add documentation for FileError types
// TODO: add abort() option to downloadFile and uploadFile methods.
// TODO: add support for downloadFile and uploadFile options. (or detailed documentation) -> for fileKey, fileName, mimeType, headers
// TODO: add support for onprogress property


    angular.module('ngCordova.plugins.file', [])

//Filesystem (checkDir, createDir, checkFile, creatFile, removeFile, writeFile, readFile)
            .factory('$cordovaFile', ['$q', function($q) {

                    return {
                        checkDir: function(dir) {
                            var q = $q.defer();

                            getFilesystem().then(
                                    function(filesystem) {
                                        filesystem.root.getDirectory(dir, {create: false},
                                        //Dir exists
                                        function() {
                                            q.resolve();
                                        },
                                                //Dir doesn't exist
                                                        function() {
                                                            q.reject();
                                                        }
                                                );
                                            }
                                    );

                                    return q.promise;
                                },
                        createDir: function(dir, replaceBOOL) {
                            getFilesystem().then(
                                    function(filesystem) {
                                        filesystem.root.getDirectory(dir, {create: true, exclusive: replaceBOOL});
                                    }
                            );
                        },
                        checkFile: function(filePath) {
                            var q = $q.defer();

                            // Backward compatibility for previous function checkFile(dir, file)
                            if (arguments.length == 2) {
                                filePath = '/' + filePath + '/' + arguments[1];
                            }

                            getFilesystem().then(
                                    function(filesystem) {
                                        filesystem.root.getFile(filePath, {create: false},
                                        // File exists
                                        function() {
                                            q.resolve();
                                        },
                                                // File doesn't exist
                                                        function() {
                                                            q.reject();
                                                        }
                                                );
                                            }
                                    );

                                    return q.promise;
                                },
                        createFile: function(filePath, replaceBOOL) {
                            // Backward compatibility for previous function createFile(dir, file, replaceBOOL)
                            if (arguments.length == 3) {
                                filePath = '/' + filePath + '/' + arguments[1];
                                replaceBOOL = arguments[2];
                            }

                            getFilesystem().then(
                                    function(filesystem) {
                                        filesystem.root.getFile(filePath, {create: true, exclusive: replaceBOOL},
                                        function(success) {

                                        },
                                                function(err) {

                                                });
                                    }
                            );
                        },
                        removeFile: function(filePath) {
                            var q = $q.defer();

                            // Backward compatibility for previous function removeFile(dir, file)
                            if (arguments.length == 2) {
                                filePath = '/' + filePath + '/' + arguments[1];
                            }

                            getFilesystem().then(
                                    function(filesystem) {
                                        filesystem.root.getFile(filePath, {create: false}, function(fileEntry) {
                                            fileEntry.remove(function() {
                                                q.resolve();
                                            });
                                        });
                                    }
                            );

                            return q.promise;
                        },
                        writeFile: function(filePath) {
                            var q = $q.defer();

                            // Backward compatibility for previous function writeFile(dir, file)
                            if (arguments.length == 2) {
                                filePath = '/' + filePath + '/' + arguments[1];
                            }

                            getFilesystem().then(
                                    function(filesystem) {
                                        filesystem.root.getFile(filePath, {create: false},
                                        function(fileEntry) {
                                            fileEntry.createWriter(
                                                    function(fileWriter) {
                                                        q.resolve(fileWriter);
                                                    },
                                                    function(error) {
                                                        q.reject(error);
                                                    });
                                        }
                                        );
                                    }
                            );

                            return q.promise;
                        },
                        readFile: function(filePath) {
                            var q = $q.defer();

                            // Backward compatibility for previous function readFile(dir, file)
                            if (arguments.length == 2) {
                                filePath = '/' + filePath + '/' + arguments[1];
                            }

                            getFilesystem().then(
                                    function(filesystem) {

                                        filesystem.root.getFile(filePath, {create: false},
                                        // success
                                        function(fileEntry) {
                                            fileEntry.file(function(file) {
                                                var reader = new FileReader();
                                                reader.onloadend = function() {
                                                    q.resolve(this.result);
                                                };

                                                reader.readAsText(file);
                                            });
                                        },
                                                // error
                                                        function(error) {
                                                            q.reject(error);
                                                        });
                                            }
                                    );

                                    return q.promise;
                                },
                        readFileMetadata: function(filePath) {
                            var q = $q.defer();

                            getFilesystem().then(
                                    function(filesystem) {
                                        filesystem.root.getFile(filePath, {create: false},
                                        // success
                                        function(fileEntry) {
                                            fileEntry.file(function(file) {
                                                q.resolve(file);
                                            });
                                        },
                                                // error
                                                        function(error) {
                                                            q.reject(error);
                                                        });
                                            }
                                    );

                                    return q.promise;
                                },
                        downloadFile: function(source, filePath, trustAllHosts, options) {
                            var q = $q.defer();
                            var fileTransfer = new FileTransfer();
                            var uri = encodeURI(source);

                            fileTransfer.onprogress = function(progressEvent) {
                                q.notify(progressEvent);
                            };

                            fileTransfer.download(
                                    uri,
                                    filePath,
                                    function(entry) {
                                        q.resolve(entry);
                                    },
                                    function(error) {
                                        q.reject(error);
                                    },
                                    trustAllHosts, options);

                            return q.promise;
                        },
                        uploadFile: function(server, filePath, options) {
                            var q = $q.defer();
                            var fileTransfer = new FileTransfer();
                            var uri = encodeURI(server);

                            fileTransfer.onprogress = function(progressEvent) {
                                q.notify(progressEvent);
                            };

                            fileTransfer.upload(
                                    filePath,
                                    uri,
                                    function(result) {
                                        q.resolve(result);
                                    },
                                    function(error) {
                                        q.reject(error);
                                    },
                                    options)

                            return q.promise
                        }

                    };

                    function getFilesystem() {
                        var q = $q.defer();

                        window.requestFileSystem(LocalFileSystem.PERSISTENT, 1024 * 1024, function(filesystem) {
                            q.resolve(filesystem);
                        },
                                function(err) {
                                    q.reject(err);
                                });

                        return q.promise;
                    }
                }]);

            angular.module('ngCordova.plugins.keyboard', [])

                    .factory('$cordovaKeyboard', [function() {

                            return {
                                hideAccessoryBar: function(bool) {
                                    return cordova.plugins.Keyboard.hideKeyboardAccessoryBar(bool);
                                },
                                close: function() {
                                    return cordova.plugins.Keyboard.close();
                                },
                                disableScroll: function(bool) {
                                    return cordova.plugins.Keyboard.disableScroll(bool);
                                },
                                isVisible: function() {
                                    return cordova.plugins.Keyboard.isVisible
                                }

                                //TODO: add support for native.keyboardshow + native.keyboardhide
                            }
                        }]);

            angular.module('ngCordova.plugins', [
                'ngCordova.plugins.device',
                'ngCordova.plugins.splashscreen',
                'ngCordova.plugins.keyboard',
                'ngCordova.plugins.statusbar',
                'ngCordova.plugins.file',
                'ngCordova.plugins.socialSharing',
                'ngCordova.plugins.appAvailability',
                'ngCordova.plugins.camera',
                'ngCordova.plugins.spinnerDialog'
            ]);

            angular.module('ngCordova.plugins.socialSharing', [])

                    .factory('$cordovaSocialSharing', ['$q', function($q) {

                            return {
                                shareViaTwitter: function(message, image, link) {
                                    var q = $q.defer();
                                    window.plugins.socialsharing.shareViaTwitter(message, image, link,
                                            function() {
                                                q.resolve(true); // success
                                            },
                                            function() {
                                                q.reject(false); // error
                                            });
                                    return q.promise;
                                },
                                shareViaWhatsApp: function(message, image, link) {  // image ?? link ??
                                    var q = $q.defer();
                                    window.plugins.socialsharing.shareViaWhatsApp(message, image, link,
                                            function() {
                                                q.resolve(true); // success
                                            },
                                            function() {
                                                q.reject(false); // error
                                            });
                                    return q.promise;
                                },
                                shareViaFacebook: function(message, image, link) {  // image ?? link ??
                                    var q = $q.defer();
                                    window.plugins.socialsharing.shareViaFacebook(message, image, link,
                                            function() {
                                                q.resolve(true); // success
                                            },
                                            function() {
                                                q.reject(false); // error
                                            });
                                    return q.promise;
                                },
                                shareViaSMS: function(message, number) {
                                    var q = $q.defer();
                                    window.plugins.socialsharing.shareViaSMS(message, number,
                                            function() {
                                                q.resolve(true); // success
                                            },
                                            function() {
                                                q.reject(false); // error
                                            });
                                    return q.promise;
                                },
                                shareViaEmail: function(message, subject, toArr, ccArr, bccArr, file) {
                                    var q = $q.defer();
                                    window.plugins.socialsharing.shareViaEmail(message, number,
                                            function() {
                                                q.resolve(true); // success
                                            },
                                            function() {
                                                q.reject(false); // error
                                            });
                                    return q.promise;
                                },
                                canShareVia: function(social, message, image, link) {
                                    var q = $q.defer();
                                    window.plugins.socialsharing.canShareVia(social, message, image, link,
                                            function(success) {
                                                q.resolve(success); // success
                                            },
                                            function(error) {
                                                q.reject(error); // error
                                            });
                                    return q.promise;
                                }

                            }
                        }]);

            angular.module('ngCordova.plugins.splashscreen', [])

                    .factory('$cordovaSplashscreen', [function() {

                            return {
                                hide: function() {
                                    return navigator.splashscreen.hide();
                                },
                                show: function() {
                                    return navigator.splashscreen.show();
                                }
                            };

                        }]);

            angular.module('ngCordova.plugins.statusbar', [])

                    .factory('$cordovaStatusbar', [function() {

                            return {
                                overlaysWebView: function(bool) {
                                    return StatusBar.overlaysWebView(true);
                                },
                                // styles: Default, LightContent, BlackTranslucent, BlackOpaque
                                style: function(style) {
                                    switch (style) {
                                        case 0:     // Default
                                            return StatusBar.styleDefault();
                                            break;

                                        case 1:     // LightContent
                                            return StatusBar.styleLightContent();
                                            break;

                                        case 2:     // BlackTranslucent
                                            return StatusBar.styleBlackTranslucent();
                                            break;

                                        case 3:     // BlackOpaque
                                            return StatusBar.styleBlackOpaque();
                                            break;

                                        default:  // Default
                                            return StatusBar.styleDefault();
                                    }
                                },
                                // supported names: black, darkGray, lightGray, white, gray, red, green, blue, cyan, yellow, magenta, orange, purple, brown
                                styleColor: function(color) {
                                    return StatusBar.backgroundColorByName(color);
                                },
                                styleHex: function(colorHex) {
                                    return StatusBar.backgroundColorByHexString(colorHex);
                                },
                                hide: function() {
                                    return StatusBar.hide();
                                },
                                show: function() {
                                    return StatusBar.show()
                                },
                                isVisible: function() {
                                    return StatusBar.isVisible();
                                }
                            }
                        }]);

            angular.module('ngCordova.plugins.spinnerDialog', [])

                    .factory('$cordovaSpinnerDialog', [function() {

                            return {
                                show: function(title, message) {
                                    return window.plugins.spinnerDialog.show(title, message);
                                },
                                hide: function() {
                                    return window.plugins.spinnerDialog.hide();
                                }
                            }

                        }]);
        })();